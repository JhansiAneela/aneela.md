# Topic :-  ES6
># Basic Data types and Data Structures with code Samples :   
## Introduction :- 
### What is ES6 ?  
ES6 is one of the Script Programming Language. It is basically Standardized name for JavaScript. It is refers to the version of the ECMA Script Programming Language. THe version 6 is to the next version of after version 5 and it was released in 2011. ECMA Script (or) ES6 was Published in June 2015. It is also known as ECMAScript 2015.  

ECMA Script is Commonly used for client side scripting on the world wide web. It is increasing being used for writing server applications and services using Node.js.  

ES6 is based on Data types and Data Structures.  
# Data Types : -  
A data type is defined as a datatype and it holda a variable and it is an attribute which tells to the compiler or interpretor how the programmer plan to do the code. 
### In JavaScript, the data types are Classified in to Seven types:-  
### These are primarily classified in to two types :-  
1. Primitive Data type
2. Non-Primitive Data type  
# Primitive Data Type :- 
In JavaScript, a primitive data type is a data type. It is not an object  and has no menthods.

1. String
2. NUmber
3. Boolean
4. Undefined
5. NUll
6. Symbol  
## String :-   
A String is a group of characters and it is enclosed with single quotes, double quotes or back ticks. The main thing is strings are immutable.
### Example :-
>var a = 'Jhansi Aneela' ;   
var b = "Jhansi Aneela" ;  
var c = 'Jhansi Aneela' ;   

Alternatively, by using String () function to create a string.   
>var d = String("hello"); 
## Number :-   
A number is a data type and it can be an integer, float number, exponential value and infinity.   
## Example :- 
>var a = 100;  // This is an integer type.  
var b = 25.5;  // This a float point.  
var c = 4e;  // This is exponential.  
Infinity :- If a number is divided by '0' then the result is infinity.    
## Boolean :-  
In generally, boolean data type has two values "TRue" and "False" which is used to check logical conditions.  
## EXample :-  
>var a = 6;  
var b = 5;  
if (a==b){  
    return True ; // Condition 1 is true.  
}
elif (a>b){}  
    return True ;  // Condition 2 is true.  
}
else{    
return False ;  // if both conditions are not satisfied then exeucte else block.  
}  

We can create a new Boolean varaiable using the Boolean function.  
>var a = Boolean (expression) ; // If the expression evaluated is true then the value 'a' is true or it is false.  

A Boolean object can be created by using the new operator.  
>var booleanobj = new Boolean (true) ;


## Undefined :-  
A undefined data type is a variable that is not defined. It is declared but it does not contain any value.  
## Examplae :-  
>var a;  
Console.log(a); // The value is undefined.  
## NUll :-   
NUll is one of the data type in JavaScript. In this we have only one value that is called "Null".  
## Example :-
>var a = "Null";  
Console.log("NUll"); // This gives NUll.  
## Symbol : -  
The symbol data type is newly added to the ES6 and it is one of the new features in ES6. THe symbol data type is a property of the object which is private to the object. It refers to the key and key value pair in the object.  
## Example :-  
>var a = { name : "Jhansi"; age : "22"; city : "Guntur"};  
var b = Symbol ("Engineer");  

The function Symbol () is used to create a Symbol.  
> var b = Symbol("Engineer");  
var b = Symbol();

                 


# Non-Primitive Data Type :-  
Non-Primitive data types are called reference types that are created by the programmer and they are not predefined. In JavaScript, arrays and functions are also comes under objects.  
1. Objects  
## Objects :-  
An object contains key-value pairs in its address.  
## Example : -  
>var k = { a : 6 , b : 8};  
We can change the value or mute the value.  
>a = 7 ;
console.log(a) // it returns (a = 7 : b = 8)  
typeof (a) // it gives type  
## Arrays : -  
Array is an object data type and it contains more than one value with  numerical values and index starts with "0".    
## Example :-  
>var a = { 2,3,4,5,6};   
We have to change the value in arrays.  
a[1]= 10;  
console.log(a) // it gives (10,2,3,4,5,6)  
## Functions :-  
Actually, Function concept doesnot have an object data type. BUt we have to fine the datatype of a function using the type of operatorthen we find that it returns a function.  
## Example :-   
>function a() {}  
typeof (a) // THis will returns type,  
# Data Structures : -  
There are four basic data structures in JavaScript. They are : -  
1. Array   
2. Queues  
3. Linked lists  
4. Trees  
5. Graphs  
6. Hashtable
## Array :-  
Array is the basic data structure and array stores the data in memory. Each array has a fixed number of values and is is started with indices "0".  
## Example :-  
>var a = | 2 | 3 | 4 |5 |;  

![](https://docs.oracle.com/javase/tutorial/figures/java/objects-tenElementArray.gif)
## Queues : -  
Conceptually, Queues are similar to stacks. But Queue follow one format to enter the elements. Queue follows FIFO (First In First Out). We have to insert the elements in front and that the same element will be delete in rear. IN queue, both inserting and deleting operations are performend at the same time.   
## Example :-  
![](https://static.studytonight.com/data-structures/images/introduction-to-queue.png)  
## Linked List :-  
Linked list is one of the data structure and it is similar to array. In linked list elements are not stored in particular memory location or index.  

Each element contains two nodes they are data stored and a link to the next node. In this the data can be any valid data type.  
## EXample :-  
![](https://media.geeksforgeeks.org/wp-content/cdn-uploads/gq/2013/03/Linkedlist.png)   

The entry point in the linked lst is called head. The head is referred to the first node in the linked list. THe last node on the list points to null. If a list is empty, the head is a null reference. 

>// User defined class node  
class Node {    
    // constructor
    constructor(element)
    {  
        this.element = element;  
        this.next = null  
    }  
}  
## Trees : -  
Tree is one of the basic structure. In tree concept, we have root node, left node and right node. In trees left node and right node are considered as left child and right child. Root node is considered as a Parent node.  

![](https://cdn.programiz.com/sites/tutorial2program/files/tree_0.png)    
The above figure shows example of tree structure.  

// Node class  
class Node  
{  
    constructor(data)  
    {  
        this.data = data;  
        this.left = null;  
        this.right = null;  
    }  
}  

## Graphs :-  
A graph is a data Structure where a node can have zero or more adjacent elements. The connection between two nodes is called edge. Nodes can also called vertices. The degree is the number of edges connected to a vertex.  

![](https://media.geeksforgeeks.org/wp-content/cdn-uploads/undirectedgraph.png)  

class Graph{  
  constructor(vertices){  
    //Total number of vertices in the graph  
    this.vertices=vertices;  
    //Defining an array which can hold LinkedLists equal to the number of vertices in the graph  
    this.list=[];  
    //Creating a new LinkedList for each vertex/index of the list  
    for(i=0; i<vertices.length; i++){  
      let temp=new LinkedList();  
      this.list.push(temp);  
    }  
  }  
}  

## Hashtables :-  
Hash table is a data structure which stores data in associative manner. In a hash table data is stored in an arrayformat, where each data value has its own unique index value.  

![](https://www.educative.io/api/page/4910944193871872/image/download/4810203387133952)  

class HashTable {  
 constructor() {  
   this.values = {};  
   this.length =  0;    
   this.size =  0;  
 }    
}  


      











